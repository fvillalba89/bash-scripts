#!/bin/bash
# Script para bloquear las ips
# Ferran Villalba Castillo
for ip in `cat /auditoria/ips`
do
	/sbin/iptables -I INPUT -s $ip -j DROP
done