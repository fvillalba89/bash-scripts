#!/bin/bash
# Script para ampliar el tamaño del directorio /tmp en cPanel & WHM
# Ferran Villalba Castillo
# De 512 MiB lo amplia a 4000 MiB | Nota: El tamaño máximo soportado en cPanel & WHM es de 4000 MiB en /tmp
/bin/systemctl stop httpd.service
/bin/systemctl stop lsws.service
/bin/systemctl stop mysql.service
/bin/systemctl stop cpanel.service
cp -rfp /tmp /tmp-backup
cp -rfp /var/tmp /tmp-backup
sed -i 's/512000/4000000/g' /usr/local/cpanel/scripts/securetmp
umount -l /tmp
umount -l /var/tmp
rm -fv /usr/tmpDSK
/scripts/securetmp
/bin/systemctl start httpd.service
/bin/systemctl start lsws.service
/bin/systemctl start mysql.service
/bin/systemctl start cpanel.service