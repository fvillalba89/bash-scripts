#!/bin/bash
# Script para instalar el repo EPEL en CentOS 7 de forma segura
# Ferran Villalba Castillo
if [ ! -f /etc/yum.repos.d/epel.repo ]; then
	yum -y install epel-release
	yum repolist
	sed -i.bak 's/enabled=1/enabled=0/g' /etc/yum.repos.d/epel.repo
fi

exit 0