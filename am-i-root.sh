#!/bin/bash
# Script para comprobar si somos root
# Ferran Villalba Castillo
if [ "$EUID" -ne "$ROOT_UID" ];
then
  echo "Eres root."
else
  echo "Eres un usuario estándar."
fi

exit 0

