#!/bin/bash
# Script para instalar y configurar CageFS en CloudLinux
# Ferran Villalba Castillo
yum install -y lvemanager cagefs
yum groupinstall -y alt-php
yum update cagefs lvemanager
cagefsctl --init
cagefsctl --enable-all
cagefsctl --addrpm rsync ftp sftp
cagefsctl --force-update
# Habilitar la latencia para IOLimits y configurar en 10 segundos
echo 10000 > /sys/module/kmodlve/parameters/latency